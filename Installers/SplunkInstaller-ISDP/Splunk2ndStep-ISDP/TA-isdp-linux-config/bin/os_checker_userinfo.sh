#!/bin/bash
#######################################
### CREATED BY: RKR			        ###
### DATE CREATED: 2020-09-08        ###
### MODIFIED BY: RKR		        ###
### DATE MODIFIED: 2021-02-16       ###
#######################################
#START=$(date +%s)
os[1]="CENTOS";
os[2]="UBUNTU";
os[3]="RED HAT";
os[4]="SUSE";
os[5]="ORACLESERVER";
os[6]="EULEROS";
os[7]="AMAZON";
os[8]="ORACLE LINUX SERVER";

function getOS()
{
	os="$1"

	hctl_checker=`hostnamectl 2>&1`;
	hctl_checker_stat="$?";
	lsb_checker=`lsb_release -a 2>&1`;
	lsb_checker_stat="$?";
	etc_checker=`cat /etc/*-release 2>&1`;
	etc_checker_stat="$?";

	if [ "$hctl_checker_stat" -eq 0 ]; then 
		resOS=`hostnamectl | grep -w 'Operating System:' | sed "s/Operating System://" | sed -e 's/^[[:space:]]*//' 2>&1`;
	elif [ "$lsb_checker_stat" -eq 0 ]; then 
		resOS=`lsb_release -si 2>&1`;
	elif [ "$etc_checker_stat" -eq 0 ]; then 
		resOS=`cat /etc/*-release | grep -w NAME | sed -e "s/NAME=//" | tr -d \" 2>&1`;
		if [ -z "$resOS" ]; then 
			resOS=`sed -n '1p' /etc/*release 2>&1`;
		fi
	else
		resOS="Unable to get OS and Version";
	fi

	for((cnt=1; cnt<=${#os[@]}; cnt++));
    do
		grpOsRes=$(echo "${resOS}" | grep -io "${os[$cnt]}" 2>&1);
		#OsUpCase="${grpOsRes^^}"
		#OsUpCase="${grpOsRes^^}"
		OsUpCase=`echo $grpOsRes | tr '[a-z]' '[A-Z]'`
		#echo "${OsUpCase}"
		
        if [ ! -z "${OsUpCase}" ]; then
            echo "${OsUpCase}";
        fi
    done

	echo "${OsUpCase}";
}
arrOS=$(getOS "${os[@]}")


function rhelUser()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
userCount=$(getent passwd | wc -l 2>&1);


results=();

for (( ctr=1; ctr<=$userCount; ctr++ ))
do  
	user_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' 2>&1`;	
	user_count_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' | wc -l 2>&1`;
	if [ $user_count_NR -eq 1 ]; then
		uID=`id -u "${user_NR}" 2>&1`;
#UserFullName		
		userFullN_checker=`getent passwd "${user_NR}" | awk -F":" '{print $5}' 2>&1`;
		userFullN_checker_stat="$?";
		
		if [[ "$userFullN_checker_stat" -eq 0  && ! -z "$userFullN_checker" ]]; then
				userFN=`getent passwd "${user_NR}" | awk -F":" '{print $5}' | sed 's/|//' 2>&1`;
		else
				userFN="N/A";
		fi
#AdminRole		
		adRole_checker=`cat /etc/sudoers | grep "${user_NR}" | wc -l 2>&1`;
		grepres=`groups "${user_NR}" | awk -F":" '{print $2}' 2>&1`;
		grepRes_checker=$(echo "$grepres" | grep "root\|wheel\|sudoers\|sudoer\|sudo" 2>&1);
		
		if [ "$adRole_checker" -eq 1 ]; then
				adminRole="Yes";
			elif [[ ! -z "${grepRes_checker}" ]]; then	
				adminRole="Yes";
		else
				adminRole="No";
		fi
#Groups
		groups=`groups "${user_NR}" | awk -F":" '{print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
#Status
		status=`passwd -S "${user_NR}" | cut -d' ' -f8- | sed 's/|//' 2>&1`;
#Commandshell
		commandshell=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $7}' | sed 's/|//' 2>&1`;
#ExpirationDate
		expiration_checker=`chage -l "${user_NR}" | grep -w 'Password expires' | wc -l 2>&1`;
		
		if [ "$expiration_checker" -eq 1 ]; then
				expiration=`chage -l "${user_NR}" | grep -w 'Password expires' | awk -F":" '/Password expires/ {print $2}' | sed -e 's/^[[:space:]]*//'  | sed 's/|//' 2>&1`;
		else
				expiration="N/A";
		fi
#Lastlogon		
		lastlogon=`lastlog -u "${user_NR}" | awk 'NR==2 {for (i=2; i<=NF; i++) printf $i FS}' | sed -e 's/[[:space:]]*$//' | sed 's/|//' 2>&1`;
#Lastpwchange
		lastpwchange_checker=`chage -l "${user_NR}" | grep -w 'Last password change' | wc -l  2>&1`;
		
		if [ "$lastpwchange_checker" -eq 1 ]; then
				lastpwchange=`chage -l "${user_NR}" | grep -w 'Last password change' | awk -F":" '/Last password change/ {print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				lastpwchange="N/A";
		fi
#AccountType		
		accnttype="Local";
#PassRequired		
		passrequired="N/A";
		nl=$'\n'
		results+="${cDate}|${uID}|${user_NR}|${userFN}|${adminRole}|${groups}|${status}|${commandshell}|${expiration}|${lastlogon}|${lastpwchange}|${accnttype}|${passrequired} ${nl}";
	fi;	
done
finalresult=`echo "${results[@]}" | sed 's/\n$//g'`;
echo "${finalresult}";

}

function centosUser()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
userCount=$(getent passwd | wc -l 2>&1);

results=();

for (( ctr=1; ctr<=$userCount; ctr++ ))
do  
	user_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' 2>&1`;	
	user_count_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' | wc -l 2>&1`;
	if [ $user_count_NR -eq 1 ]; then
		uID=`id -u "${user_NR}" 2>&1`;
#UserFullName		
		userFullN_checker=`getent passwd "${user_NR}" | awk -F":" '{print $5}'  2>&1`;
		userFullN_checker_stat="$?";
		
		if [[ "$userFullN_checker_stat" -eq 0  && ! -z "$userFullN_checker" ]]; then
				 =`getent passwd "${user_NR}" | awk -F":" '{print $5}' | sed 's/|//' 2>&1`;
		else
				userFN="N/A";
		fi
#AdminRole		
		adRole_checker=`cat /etc/sudoers | grep "${user_NR}" | wc -l 2>&1`;
		grepres=`groups "${user_NR}" | awk -F":" '{print $2}' 2>&1`;
		grepRes_checker=$(echo "$grepres" | grep "root\|wheel\|sudoers\|sudoer\|sudo" 2>&1);
		
		if [ "$adRole_checker" -eq 1 ]; then
				adminRole="Yes";
			elif [[ ! -z "${grepRes_checker}" ]]; then	
				adminRole="Yes";
		else
				adminRole="No";
		fi
#Groups
		groups=`groups "${user_NR}" | awk -F":" '{print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
#Status
		status=`passwd -S "${user_NR}" | cut -d' ' -f8- | sed 's/|//' 2>&1`;
#Commandshell
		commandshell=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $7}' | sed 's/|//' 2>&1`;
#ExpirationDate
		expiration_checker=`chage -l "${user_NR}" | grep -w 'Password expires' | wc -l 2>&1`;
		
		if [ "$expiration_checker" -eq 1 ]; then
				expiration=`chage -l "${user_NR}" | grep -w 'Password expires' | awk -F":" '/Password expires/ {print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				expiration="N/A";
		fi
#LastLogon
		lastlogon=`lastlog -u "${user_NR}" | awk 'NR==2 {for (i=2; i<=NF; i++) printf $i FS}' | sed -e 's/[[:space:]]*$//' | sed 's/|//' 2>&1`;
#Lastpwchange
		lastpwchange_checker=`chage -l "${user_NR}" | grep -w 'Last password change' | wc -l 2>&1`;
		
		if [ "$lastpwchange_checker" -eq 1 ]; then
				lastpwchange=`chage -l "${user_NR}" | grep -w 'Last password change' | awk -F":" '/Last password change/ {print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				lastpwchange="N/A";
		fi
#AccountType		
		accnttype="Local";
#PassRequired		
		passrequired="N/A";
		nl=$'\n'
		results+="${cDate}|${uID}|${user_NR}|${userFN}|${adminRole}|${groups}|${status}|${commandshell}|${expiration}|${lastlogon}|${lastpwchange}|${accnttype}|${passrequired} ${nl}";
	fi;
done
finalresult=`echo "${results[@]}" | sed 's/\n$//g'`;
echo "${finalresult}";

}

function ubuntuUser()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
userCount=$(getent passwd | wc -l 2>&1);

results=();

for (( ctr=1; ctr<=$userCount; ctr++ ))
do  
	user_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' 2>&1`;	
	user_count_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' | wc -l 2>&1`;
	if [ $user_count_NR -eq 1 ]; then
		uID=`id -u "${user_NR}" 2>&1`;
#UserFullName		
		userFullN_checker=`getent passwd "${user_NR}" | awk -F":" '{print $5}' 2>&1`;
		userFullN_checker_stat="$?";
		
		if [[ "$userFullN_checker_stat" -eq 0  && ! -z "$userFullN_checker" ]]; then
				userFN=`getent passwd "${user_NR}" | awk -F":" '{print $5}' | sed 's/|//' 2>&1`;
		else
				userFN="N/A";
		fi
#AdminRole		
		adRole_checker=`cat /etc/sudoers | grep "${user_NR}" | wc -l 2>&1`;
		grepres=`groups "${user_NR}" | awk -F":" '{print $2}' 2>&1`;
		grepRes_checker=$(echo "$grepres" | grep "root\|wheel\|sudoers\|sudoer\|sudo" 2>&1);
		
		if [ "$adRole_checker" -eq 1 ]; then
				adminRole="Yes";
			elif [[ ! -z "${grepRes_checker}" ]]; then	
				adminRole="Yes";
		else
				adminRole="No";
		fi
#Groups		
		groups=`groups "${user_NR}" | awk -F":" '{print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
#Status
		statusRes=`passwd -S "${user_NR}" | awk '{print $2}' 2>&1`;
		if [ "$statusRes" = "L" ]; then 
				status="Locked";
			elif [ "$statusRes" = "NP" ]; then 
				status="No Password";
			elif [ "$statusRes" = "P" ]; then
				status="Password Set";
		else
				status="Unable to find status";
		fi
#Commandshell		
		commandshell=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $7}' | sed 's/|//' 2>&1`;
#ExpirationDate
		expiration_checker=`chage -l "${user_NR}" | grep -w 'Password expires' | wc -l 2>&1`;
		
		if [ "$expiration_checker" -eq 1 ]; then		
				expiration=`chage -l "${user_NR}" | grep -w 'Password expires' | awk -F":" '/Password expires/ {print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				expiration="N/A";
		fi
#LastLogon		
		lastlogon=`lastlog -u "${user_NR}" | awk 'NR==2 {for (i=2; i<=NF; i++) printf $i FS}' | sed -e 's/[[:space:]]*$//' | sed 's/|//' 2>&1`;
#Lastpwchange		
		lastpwchange_checker=`chage -l "${user_NR}" | grep -w 'Last password change' | wc -l 2>&1`;
		
		if [ "$lastpwchange_checker" -eq 1 ]; then
				lastpwchange=`chage -l "${user_NR}" | grep -w 'Last password change' | awk -F":" '/Last password change/ {print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				lastpwchange="N/A";
		fi
#AccountType		
		accnttype="Local";
#PassRequired		
		passrequired="N/A";
		nl=$'\n'
		results+="${cDate}|${uID}|${user_NR}|${userFN}|${adminRole}|${groups}|${status}|${commandshell}|${expiration}|${lastlogon}|${lastpwchange}|${accnttype}|${passrequired} ${nl}";
	fi;
done
finalresult=`echo "${results[@]}" | sed 's/\n$//g'`;
echo "${finalresult}";

}

function suseUser()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
userCount=$(getent passwd | wc -l 2>&1);

results=();

for (( ctr=1; ctr<=$userCount; ctr++ ))
do  
	user_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' 2>&1`;	
	user_count_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' | wc -l 2>&1`;
	if [ $user_count_NR -eq 1 ]; then
		uID=`id -u "${user_NR}" 2>&1`;
#UserFullName		
		userFullN_checker=`getent passwd "${user_NR}" | awk -F":" '{print $5}' 2>&1`;
		userFullN_checker_stat="$?";
		
		if [[ "$userFullN_checker_stat" -eq 0  && ! -z "$userFullN_checker" ]]; then
				userFN=`getent passwd "${user_NR}" | awk -F":" '{print $5}' | sed 's/|//' 2>&1`;
		else
				userFN="N/A";
		fi
#AdminRole		
		adRole_checker=`cat /etc/sudoers | grep "${user_NR}" | wc -l 2>&1`;
		grepres=`groups "${user_NR}" | awk -F":" '{print $2}' 2>&1`;
		grepRes_checker=$(echo "$grepres" | grep "root\|wheel\|sudoers\|sudoer\|sudo" 2>&1);
		
		if [ "$adRole_checker" -eq 1 ]; then
				adminRole="Yes";
			elif [[ ! -z "${grepRes_checker}" ]]; then	
				adminRole="Yes";
		else
				adminRole="No";
		fi
#Groups		
		groups=`groups "${user_NR}" | awk -F":" '{print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
#Status
		statusRes=`passwd -S "${user_NR}" | awk '{print $2}' 2>&1`;
		if [ "$statusRes" = "LK" ]; then 
				status="Locked";
			elif [ "$statusRes" = "NP" ]; then 
				status="No Password";
			elif [ "$statusRes" = "PS" ]; then
				status="Password Set";
		else
				status="Unable to find status";
		fi
#commandshell		
		commandshell=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $7}' | sed 's/|//' 2>&1`;
#ExpirationDate		
		expiration_checker=`chage -l "${user_NR}" | grep -w 'Password Expires:' | wc -l 2>&1`;
		
		if [ "$expiration_checker" -eq 1 ]; then
				expiration=`chage -l "${user_NR}" | grep -w 'Password Expires:' | sed "s/Password Expires://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				expiration="N/A";
		fi
#LastLogon	
		lastlogon=`lastlog -u "${user_NR}" | awk 'NR==2 {for (i=2; i<=NF; i++) printf $i FS}' | sed -e 's/[[:space:]]*$//' | sed 's/|//' 2>&1`;
#lastpwchange	
		lastpwchange_checker=`chage -l "${user_NR}" | grep -w 'Last Change:' | wc -l 2>&1`;
		
		if [ "$lastpwchange_checker" -eq 1 ]; then
				lastpwchange=`chage -l "${user_NR}" | grep -w 'Last Change:' | sed "s/Last Change://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				lastpwchange="N/A";
		fi
#AccountType		
		accnttype="Local";
#PassRequired		
		passrequired="N/A";
		nl=$'\n'
		results+="${cDate}|${uID}|${user_NR}|${userFN}|${adminRole}|${groups}|${status}|${commandshell}|${expiration}|${lastlogon}|${lastpwchange}|${accnttype}|${passrequired} ${nl}";
	fi;
done
finalresult=`echo "${results[@]}" | sed 's/\n$//g'`;
echo "${finalresult}";

}

function oracleUser()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
userCount=$(getent passwd | wc -l 2>&1);


results=();

for (( ctr=1; ctr<=$userCount; ctr++ ))
do  
	user_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' 2>&1`;	
	user_count_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' | wc -l 2>&1`;
	if [ $user_count_NR -eq 1 ]; then
		uID=`id -u "${user_NR}" 2>&1`;
#UserFullName		
		userFullN_checker=`getent passwd "${user_NR}" | awk -F":" '{print $5}' 2>&1`;
		userFullN_checker_stat="$?";
		
		if [[ "$userFullN_checker_stat" -eq 0  && ! -z "$userFullN_checker" ]]; then
				userFN=`getent passwd "${user_NR}" | awk -F":" '{print $5}' | sed 's/|//' 2>&1`;
		else
				userFN="N/A";
		fi
#AdminRole		
		adRole_checker=`cat /etc/sudoers | grep "${user_NR}" | wc -l 2>&1`;
		grepres=`groups "${user_NR}" | awk -F":" '{print $2}' 2>&1`;
		grepRes_checker=$(echo "$grepres" | grep "root\|wheel\|sudoers\|sudoer\|sudo" 2>&1);
		
		if [ "$adRole_checker" -eq 1 ]; then
				adminRole="Yes";
			elif [[ ! -z "${grepRes_checker}" ]]; then	
				adminRole="Yes";
		else
				adminRole="No";
		fi
#Groups		
		groups=`groups "${user_NR}" | awk -F":" '{print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
#Status
		status=`passwd -S "${user_NR}" | cut -d' ' -f8- | sed 's/|//' 2>&1`;
#Commandshell
		commandshell=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $7}' | sed 's/|//' 2>&1`;
#ExpirationDate
		expiration_checker=`chage -l "${user_NR}" | grep -w 'Password expires' | wc -l 2>&1`;
		
		if [ "$expiration_checker" -eq 1 ]; then
				expiration=`chage -l "${user_NR}" | grep -w 'Password expires' | awk -F":" '/Password expires/ {print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				expiration="N/A";
		fi
#LastLogon
		lastlogon=`lastlog -u "${user_NR}" | awk 'NR==2 {for (i=2; i<=NF; i++) printf $i FS}' | sed -e 's/[[:space:]]*$//' | sed 's/|//' 2>&1`;
#Lastpwchange
		lastpwchange_checker=`chage -l "${user_NR}" | grep -w 'Last password change' | wc -l 2>&1`;
		
		if [ "$lastpwchange_checker" -eq 1 ]; then
				lastpwchange=`chage -l "${user_NR}" | grep -w 'Last password change' | awk -F":" '/Last password change/ {print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				lastpwchange="N/A";
		fi
#AccountType
		accnttype="Local";
#PassRequired
		passrequired="N/A";
		nl=$'\n'
		results+="${cDate}|${uID}|${user_NR}|${userFN}|${adminRole}|${groups}|${status}|${commandshell}|${expiration}|${lastlogon}|${lastpwchange}|${accnttype}|${passrequired} ${nl}";
	fi;
done
finalresult=`echo "${results[@]}" | sed 's/\n$//g'`;
echo "${finalresult}";

}

function eulerUser()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
userCount=$(getent passwd | wc -l 2>&1);

results=();

for (( ctr=1; ctr<=$userCount; ctr++ ))
do  
	user_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' 2>&1`;	
	user_count_NR=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $1}' | wc -l 2>&1`;
	if [ $user_count_NR -eq 1 ]; then
		uID=`id -u "${user_NR}" 2>&1`;
#UserFullName		
		userFullN_checker=`getent passwd "${user_NR}" | awk -F":" '{print $5}' 2>&1`;
		userFullN_checker_stat="$?";
		
		if [[ "$userFullN_checker_stat" -eq 0  && ! -z "$userFullN_checker" ]]; then
				userFN=`getent passwd "${user_NR}" | awk -F":" '{print $5}' | sed 's/|//' 2>&1`;
		else
				userFN="N/A";
		fi
#AdminRole		
		adRole_checker=`cat /etc/sudoers | grep "${user_NR}" | wc -l 2>&1`;
		grepres=`groups "${user_NR}" | awk -F":" '{print $2}' 2>&1`;
		grepRes_checker=$(echo "$grepres" | grep "root\|wheel\|sudoers\|sudoer\|sudo" 2>&1);
		
		if [ "$adRole_checker" -eq 1 ]; then
				adminRole="Yes";
			elif [[ ! -z "${grepRes_checker}" ]]; then	
				adminRole="Yes";
		else
				adminRole="No";
		fi
#Groups		
		groups=`groups "${user_NR}" | awk -F":" '{print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
#Status
		status=`passwd -S "${user_NR}" | cut -d' ' -f8- | sed 's/|//' 2>&1`;
#Commandshell
		commandshell=`cat /etc/passwd | awk -F":" 'NR=='"$ctr"' {print $7}' | sed 's/|//' 2>&1`;
#ExpirationDate
		expiration_checker=`chage -l "${user_NR}" | grep -w 'Password expires' | wc -l 2>&1`;
		
		if [ "$expiration_checker" -eq 1 ]; then
				expiration=`chage -l "${user_NR}" | grep -w 'Password expires' | awk -F":" '/Password expires/ {print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				expiration="N/A";
		fi
#LastLogon
		lastlogon=`lastlog -u "${user_NR}" | awk 'NR==2 {for (i=2; i<=NF; i++) printf $i FS}' | sed -e 's/[[:space:]]*$//' | sed 's/|//' 2>&1`;
#Lastpwchange		
		lastpwchange_checker=`chage -l "${user_NR}" | grep -w 'Last password change' | wc -l 2>&1`;
		
		if [ "$lastpwchange_checker" -eq 1 ]; then
				lastpwchange=`chage -l "${user_NR}" | grep -w 'Last password change' | awk -F":" '/Last password change/ {print $2}' | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		else
				lastpwchange="N/A";
		fi
#AccountType
		accnttype="Local";
#PassRequired		
		passrequired="N/A";
		nl=$'\n'
		results+="${cDate}|${uID}|${user_NR}|${userFN}|${adminRole}|${groups}|${status}|${commandshell}|${expiration}|${lastlogon}|${lastpwchange}|${accnttype}|${passrequired} ${nl}";
	fi;
done
finalresult=`echo "${results[@]}" | sed 's/\n$//g'`;
echo "${finalresult}";

}

if [[ "${arrOS}" == "${os[1]}" ]]; then
#	echo "CENTOS LOGIC HERE";
	centosUser
elif [[ "${arrOS}" == "${os[2]}" ]]; then
#	echo "UBUNTU LOGIC HERE";
	ubuntuUser
elif [[ "${arrOS}" == "${os[3]}" ]]; then
#	echo "RED HAT LOGIC HERE";
	rhelUser
elif [[ "${arrOS}" == "${os[4]}" ]]; then
#	echo "SUSE LOGIC HERE";
	suseUser
elif [[ "${arrOS}" == "${os[5]}" ]]; then
#	echo "ORACLESERVER LOGIC HERE";
	oracleUser
elif [[ "${arrOS}" == "${os[6]}" ]]; then
#	echo "EULEROS LOGIC HERE";
	eulerUser
elif [[ "${arrOS}" == "${os[7]}" ]]; then
#	echo "AMAZON LOGIC HERE";
	rhelUser
elif [[ "${arrOS}" == "${os[8]}" ]]; then
#	echo "ORACLE LINUX SERVER LOGIC HERE";
	oracleUser
fi

#END=$(date +%s)
#DIFF=$(( $END - $START ))
#echo "It took $DIFF seconds"
